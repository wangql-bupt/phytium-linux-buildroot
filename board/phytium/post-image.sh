#!/bin/sh

BOARD_DIR="$(dirname $0)"

if grep -Eq "^BR2_TARGET_GENERIC_HOSTNAME=\"D2000\"$" ${BR2_CONFIG}; then
	cp -f ${BOARD_DIR}/grub-d2000.cfg ${BINARIES_DIR}/efi-part/EFI/BOOT/grub.cfg
else
	cp -f ${BOARD_DIR}/grub.cfg ${BINARIES_DIR}/efi-part/EFI/BOOT/grub.cfg
fi
